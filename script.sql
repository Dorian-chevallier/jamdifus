CREATE DATABASE IF NOT EXISTS `jamdifus`;

use jamdifus;

CREATE TABLE IF NOT EXISTS `telescopes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `diameter` int(11) DEFAULT NULL,
  `focal_length` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO telescopes (name, price, diameter, focal_length) VALUES ("Télescope dobson sky-watcher 203/1200", 499, 203, 1200);
INSERT INTO telescopes (name, price, diameter, focal_length) VALUES ("Télescope eVscope 2", 4699, 114, 450);
INSERT INTO telescopes (name, price, diameter, focal_length) VALUES ("Lunette astro national geographic 70/900", 204, 70, 900);
INSERT INTO telescopes (name, price, diameter, focal_length) VALUES ("Télescope Sky-Watcher 130/650 sur monture StarQuest", 299, 130, 650);
INSERT INTO telescopes (name, price, diameter, focal_length) VALUES ("Télescope Sky-Watcher Mak90 sur monture StarQuest", 309, 90, 1250);
INSERT INTO telescopes (name, price, diameter, focal_length) VALUES ("Télescope Dobson Sky-Watcher 76/300 Heritage", 98, 76, 300);
INSERT INTO telescopes (name, price, diameter, focal_length) VALUES ("Lunette 120/1000 Sky-Watcher sur équatoriale NEQ5", 860, 120, 1000);
INSERT INTO telescopes (name, price, diameter, focal_length) VALUES ("Télescope Dobson Sky-Watcher 250mm FlexTube", 899, 254, 1200);
INSERT INTO telescopes (name, price, diameter, focal_length) VALUES ("Lunette 80/400 Sky-Watcher sur monture azimutale AZ3", 280, 80, 400);
INSERT INTO telescopes (name, price, diameter, focal_length) VALUES ("Télescope Sky-Watcher Mak127 sur AZGTi", 790, 149, 1500);
INSERT INTO telescopes (name, price, diameter, focal_length) VALUES ("Télescope Sky-Watcher 150/750 sur EQ3-2 Pro Go-To Black Diamond", 995, 150, 750);
INSERT INTO telescopes (name, price, diameter, focal_length) VALUES ("Télescope Sky-Watcher 150/750 Dual Speed sur NEQ3-2 Black Diamond", 720, 150, 750);