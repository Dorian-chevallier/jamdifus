<?php

require('../vendor/somesh/php-query/phpQuery/phpQuery.php');
require('../src/SPDO.php');
require('../src/Entity/Telescope.php');

$doc = phpQuery::newDocumentFileHTML("../public/index.html");

pq(".display-telescopes")->append("<ul class='telescopes'></ul>");

// On construit notre page principale avec les données dans la base
foreach(SPDO::getInstance()->query("SELECT id, name, price FROM telescopes t") as $telescope) {
    pq(".telescopes")->append("<li class='telescope-link' id=". $telescope['id'] .">
                                    <span class='telescope-name'>" . $telescope['name'] . "</span><button class='delete-btn' id=". $telescope['id'] . ">Supprimer</button>
                                </li>");
}

print phpQuery::getDocument($doc->getDocumentID());