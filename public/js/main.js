let modal = document.getElementById("myModal");
let span = document.getElementsByClassName("close")[0];

let telescopeLink = $(".telescope-link");
let deleteBtn = $(".delete-btn");


telescopeLink.bind("click", (e) => {


    if (e.target.classList.value == "delete-btn") {
        return
    }
    // On affiche la modal
    modal.style.display = "block";

    // requête ajax pour récupérer les infos du télescope en question
    $.ajax({
        method: "POST",
        url: "../src/TelescopeController.php",
        dataType: 'json',
        data: { id: e.currentTarget.id, action: "fetchById" },
    })
        .done(function (data) {
            // on récupère l'objet Telescope pour afficher ses données dynamiquement dans la modal
            modal.getElementsByTagName("h2")[0].innerText = data.name;
            modal.getElementsByClassName("modal-body")[0].append(createNewElement("p", "Prix : " + data.price + "€"));
            modal.getElementsByClassName("modal-body")[0].append(createNewElement("p", "Diamètre du tube : " + data.diameter + "mm"));
            modal.getElementsByClassName("modal-body")[0].append(createNewElement("p", "Longueur focale : " + data.focalLength + "mm"));
            modal.getElementsByClassName("modal-body")[0].append(createNewElement("p", "Grossissement maximum théorique : x" + data.theoreticalMaximumMagnification));
        });
});

// Lorsque l'on clique sur le bouton de suppression de la liste on génère des élements HTML de confirmation
// Si on clique sur le bouton de confirmation la requête opère et si on clique sur le bouton d'annulation on ferme la modal
deleteBtn.bind("click", (e) => {
    modal.style.display = "block";
    modal.getElementsByTagName("h2")[0].innerText = "Voulez vous vraiment suprimmer ce télescope ?";
    modal.getElementsByClassName("modal-body")[0].append(createNewElement("button", "Confirmer", "btn-go"));
    modal.getElementsByClassName("modal-body")[0].append(createNewElement("button", "Annuler", "btn-cancel"));

    const confirmBtn = modal.getElementsByClassName("btn-go")[0];
    const cancelBtn = modal.getElementsByClassName("btn-cancel")[0];

    confirmBtn.addEventListener("click", function () {
        $.ajax({
            method: "POST",
            url: "../src/TelescopeController.php",
            dataType: 'json',
            data: { id: e.currentTarget.id, action: "deleteById" },
        })
            .done(function (id) {
                $("li.telescope-link#" + id).removeAttr("style").hide();
                closeModal();
            });
    });

    cancelBtn.addEventListener("click", function () {
        closeModal();
    });
})

// permet de créer un élement HTML
function createNewElement(elementTag, textContent, className = null) {
    let newElement = document.createElement(elementTag);
    newElement.textContent = textContent;
    if(className) {
        newElement.classList.add(className);
    }
    return newElement;
}

// Ferme la modal et efface les élements générés
function closeModal() {
    modal.style.display = "none";
    $.each($('.modal-body > p'), function (index, element) {
        element.remove()
    });

    $.each($('.modal-body > button'), function (index, element) {
        element.remove()
    });
}

// Ferme la modal lors du clique sur la croix
span.onclick = function () {
    closeModal()
}

// Ferme la modal lors du clique en dehors de celle-ci
window.onclick = function (event) {
    if (event.target == modal) {
        closeModal()
    }
};

// Barre de filtre par nom de télescope
searchTelescopeInput = $("#search-telescope")
searchTelescopeInput.keyup(() => {
    filter = searchTelescopeInput.val().toUpperCase();
    $.each($('.telescope-name'), function (index, element) {
        let txtValue = $(element).text();
        let telescopeLink = $(element).parent()
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            telescopeLink.show();
        } else {
            telescopeLink.removeAttr("style").hide();
        }
    });
})