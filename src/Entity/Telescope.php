<?php 

class Telescope {

    private $id;
    private $name;
    private $price;
    private $diameter;
    private $focalLength;
    private $theoreticalMaximumMagnification;

    public function __construct($id, $name, $price, $diameter, $focalLength)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->diameter = $diameter;
        $this->focalLength = $focalLength;
        $this->theoreticalMaximumMagnification = $diameter * 2;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getPrice() : float
    {
        return $this->price;
    }

    public function getDiameter() : int
    {
        return $this->diameter;
    }

    public function getFocalLength() : int
    {
        return $this->focalLength;
    }

    public function getTheoreticalMaximumMagnification() : int
    {
        return $this->theoreticalMaximumMagnification;
    }

    public function jsonSerialize() : array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'price' => $this->getPrice(),
            'diameter' => $this->getDiameter(),
            'focalLength' => $this->getFocalLength(),
            'theoreticalMaximumMagnification' => $this->getTheoreticalMaximumMagnification()
        ];
    }

}