<?php
require('./SPDO.php');
require('../src/Entity/Telescope.php');

$id = $_POST['id'];

function fetchById($id) {
    $result = SPDO::getInstance()->query("SELECT id, name, price, diameter, focal_length FROM telescopes t WHERE t.id = " . $id)->fetch();
    $telescope = new Telescope($result['id'], $result['name'], $result['price'], $result['diameter'], $result['focal_length']);

    echo json_encode($telescope->jsonSerialize());
}

function deleteById($id) {
    SPDO::getInstance()->query("DELETE FROM telescopes WHERE telescopes.id = " . $id);
    echo json_encode($id);
}

switch ($_POST['action']) {
  case "fetchById":
    fetchById($id);
    break;
  case "deleteById":
    deleteById($id);
    break;
}